package io.gitlab.chaver.chocotools.objective;

import io.gitlab.chaver.chocotools.util.Creator;
import org.chocosolver.solver.variables.IntVar;

public class KnapParetoMaximizer extends IntParetoMaximizer<KnapSol> {

    public KnapParetoMaximizer(IntVar[] objectives, Creator<KnapSol> creator, boolean strict) {
        super(objectives, creator, strict);
    }

    @Override
    protected int[] getObjValues(KnapSol sol) {
        return sol.getProfits();
    }
}
