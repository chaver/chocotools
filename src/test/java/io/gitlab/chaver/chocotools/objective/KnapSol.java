package io.gitlab.chaver.chocotools.objective;

public class KnapSol {

    private int[] profits;

    public KnapSol(int[] profits) {
        this.profits = profits;
    }

    public int[] getProfits() {
        return profits;
    }
}
