package io.gitlab.chaver.chocotools.objective;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntParetoMaximizerTest {

    private int nbItems = 10;
    private int[][] weights = {
            {9, 8, 2, 7, 3, 6, 1, 3, 9, 3},
            {3, 4, 2, 4, 9, 5, 4, 8, 3, 7}
    };

    private int[][] profits = {
            {2, 7, 4, 5, 6, 2, 7, 3, 7, 1},
            {3, 9, 1, 5, 3, 8, 2, 6, 1, 3}
    };
    private int[] maxCapacity = {38, 35};

    private int[][] expectedParetoSet = {
            {39, 27},
            {38, 29},
            {36, 30},
            {35, 32},
            {34, 33},
            {32, 34},
            {29, 35},
            {27, 36}
    };

    /**
     * The data come from the following paper :
     *  Christine Mumford - A Simple Approach to Evolutionary Multiobjective Optimization
     */
    @Test
    void testParetoKnapsack() {
        Model model = new Model("Pareto knapsack");
        BoolVar[] x = model.boolVarArray("x", nbItems);
        IntVar[] profit = model.intVarArray("profit", maxCapacity.length, 0, IntVar.MAX_INT_BOUND);
        IntVar[] capacity = new IntVar[maxCapacity.length];
        for (int i = 0; i < maxCapacity.length; i++) {
            model.scalar(x, profits[i], "=", profit[i]).post();
            capacity[i] = model.intVar("capacity" + i, 0, maxCapacity[i]);
            model.scalar(x, weights[i], "=", capacity[i]).post();
        }
        KnapSolCreator creator = new KnapSolCreator(profit);
        KnapParetoMaximizer maximizer = new KnapParetoMaximizer(profit, creator, false);
        model.post(new Constraint("Pareto", maximizer));
        model.getSolver().plugMonitor(maximizer);
        while (model.getSolver().solve());
        assertEquals(expectedParetoSet.length, maximizer.getSolutions().size());
        maximizer.getSolutions().sort(Comparator.comparingInt(o -> -o.getProfits()[0]));
        for (int i = 0; i < expectedParetoSet.length; i++) {
            assertArrayEquals(expectedParetoSet[i], maximizer.getSolutions().get(i).getProfits());
        }
    }
}
