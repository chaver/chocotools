package io.gitlab.chaver.chocotools.objective;

import io.gitlab.chaver.chocotools.util.Creator;
import org.chocosolver.solver.variables.IntVar;

import java.util.Arrays;

public class KnapSolCreator implements Creator<KnapSol> {

    private IntVar[] profit;

    public KnapSolCreator(IntVar[] profit) {
        this.profit = profit;
    }

    @Override
    public KnapSol create() {
        return new KnapSol(Arrays.stream(profit).mapToInt(IntVar::getValue).toArray());
    }
}
