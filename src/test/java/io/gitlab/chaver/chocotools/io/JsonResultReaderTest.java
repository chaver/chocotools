package io.gitlab.chaver.chocotools.io;

import io.gitlab.chaver.chocotools.problem.SolutionExample;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class JsonResultReaderTest {

    private final String rscPath = "src/test/resources/";

    @Test
    void testReadWithSols() throws IOException {
        ProblemResultReader<SolutionExample, MeasuresView> reader = new JsonResultReader<>(rscPath + "res.json");
        ProblemResult<SolutionExample, MeasuresView> res = reader.readResult(SolutionExample[].class, MeasuresView.class);
        assertEquals(3, res.getSolutions().size());
        assertEquals(3, res.getProperties().getSolutionCount());
    }

    @Test
    void testReadWithoutSols() throws IOException {
        ProblemResultReader<SolutionExample, MeasuresView> reader = new JsonResultReader<>(rscPath + "res_without_sol.json");
        ProblemResult<SolutionExample, MeasuresView> res = reader.readResult(SolutionExample[].class, MeasuresView.class);
        assertNull(res.getSolutions());
    }
}
