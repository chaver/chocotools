package io.gitlab.chaver.chocotools.io;

import io.gitlab.chaver.chocotools.problem.ChocoProblemExample;
import io.gitlab.chaver.chocotools.problem.SolutionExample;
import org.junit.jupiter.api.Test;
import picocli.CommandLine;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonLinesTest {

    @Test
    void test() throws Exception {
        File resFile = File.createTempFile("res", "");
        ChocoProblemExample ex = new ChocoProblemExample();
        new CommandLine(ex).execute("--jsonl", resFile.getAbsolutePath());
        ProblemResultReader<SolutionExample, MeasuresView> reader = new JsonLinesResultReader<>(resFile.getAbsolutePath(),
                SolutionExample.class);
        ProblemResult<SolutionExample, MeasuresView> res = reader.readResult(SolutionExample[].class, MeasuresView.class);
        for (int i = 0; i < res.getSolutions().size(); i++) {
            assertEquals(res.getSolutions().get(i), ex.getSolutions().get(i));
        }
        assertEquals(5, res.getProperties().getNodeCount());
    }
}
