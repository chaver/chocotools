package io.gitlab.chaver.chocotools.problem;


import io.gitlab.chaver.chocotools.io.ProblemResult;
import io.gitlab.chaver.chocotools.io.ProblemResultReader;
import io.gitlab.chaver.chocotools.io.JsonResultReader;
import io.gitlab.chaver.chocotools.io.MeasuresView;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChocoProblemTest {

    @TempDir
    File tempDir;

    @Test
    void testChocoProblem() throws IOException {
        File resFile = new File(tempDir, "res.json");
        ChocoProblemExample problem = new ChocoProblemExample();
        new CommandLine(problem).execute("--json", resFile.getAbsolutePath());
        assertEquals(3, problem.getSolutions().size());
        assertTrue(resFile.exists());
        ProblemResultReader<SolutionExample, MeasuresView> reader = new JsonResultReader<>(resFile.getAbsolutePath());
        ProblemResult<SolutionExample, MeasuresView> res = reader.readResult(SolutionExample[].class, MeasuresView.class);
        for (int i = 0; i < res.getSolutions().size(); i++) {
            assertEquals(res.getSolutions().get(i), problem.getSolutions().get(i));
        }
        assertEquals(5, res.getProperties().getNodeCount());
    }
}
