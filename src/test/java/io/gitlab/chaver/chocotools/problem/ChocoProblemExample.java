package io.gitlab.chaver.chocotools.problem;

import io.gitlab.chaver.chocotools.io.MeasuresView;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;

import java.util.LinkedList;
import java.util.List;

public class ChocoProblemExample extends ChocoProblem<SolutionExample, MeasuresView> {

    private static class SolMonitor implements IMonitorSolution {

        private IntVar x;
        private IntVar y;
        private List<SolutionExample> sols = new LinkedList<>();

        public SolMonitor(IntVar x, IntVar y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void onSolution() {
            sols.add(new SolutionExample(x.getValue(), y.getValue()));
        }

        public List<SolutionExample> getSols() {
            return sols;
        }
    }

    private SolMonitor solMonitor;

    @Override
    public List<SolutionExample> getSolutions() {
        return solMonitor.getSols();
    }

    @Override
    public MeasuresView getProperties() {
        return getSearchStats();
    }

    @Override
    public void parseArgs() {}

    @Override
    public void buildModel() {
        IntVar x = model.intVar("x", 1, 3);
        IntVar y = model.intVar("y", 2, 3);
        x.lt(y).post();
        solMonitor = new SolMonitor(x, y);
        solver.plugMonitor(solMonitor);
    }

    @Override
    protected Model createModel() {
        return new Model("example");
    }
}
