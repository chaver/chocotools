package io.gitlab.chaver.chocotools.search.loop.monitors;

import io.gitlab.chaver.chocotools.util.Creator;
import io.gitlab.chaver.chocotools.util.ISolutionProvider;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;

import java.util.LinkedList;
import java.util.List;

/**
 * A simple monitor to record solutions during the search (see the Creator class)
 * @param <S> solution type
 */
public class SolutionRecorderMonitor<S> implements IMonitorSolution, ISolutionProvider<S> {

    private Creator<S> creator;
    private List<S> solutions = new LinkedList<>();

    public SolutionRecorderMonitor(Creator<S> creator) {
        this.creator = creator;
    }

    @Override
    public List<S> getSolutions() {
        return solutions;
    }

    @Override
    public void onSolution() {
        solutions.add(creator.create());
    }
}
