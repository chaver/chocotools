package io.gitlab.chaver.chocotools.util;

import io.gitlab.chaver.chocotools.problem.BuildModelException;
import io.gitlab.chaver.chocotools.problem.OutputException;
import io.gitlab.chaver.chocotools.problem.SetUpException;
import io.gitlab.chaver.chocotools.problem.SolvingException;
import picocli.CommandLine;
import picocli.CommandLine.IExecutionExceptionHandler;
import picocli.CommandLine.ParseResult;

import java.io.PrintWriter;

/**
 * Exception handler in problem (production mode)
 */
public class ProblemExceptionHandlerProd implements IExecutionExceptionHandler {

    @Override
    public int handleExecutionException(Exception e, CommandLine cmd,
                                        ParseResult parseResult) throws Exception {
        PrintWriter err = cmd.getErr();
        if (e.getClass() == SetUpException.class) {
            err.println(cmd.getColorScheme().errorText(e.getMessage()));
            err.println(cmd.getColorScheme().errorText(cmd.getUsageMessage()));
        }
        else if (e.getClass() == BuildModelException.class) {
            err.println(cmd.getColorScheme().errorText("Error while building the model : " + e.getMessage()));
        }
        else if (e.getClass() == SolvingException.class) {
            err.println(cmd.getColorScheme().errorText("Error while solving the problem : " + e.getMessage()));
        }
        else if (e.getClass() == OutputException.class) {
            err.println(cmd.getColorScheme().errorText("Error while outputting the result : " + e.getMessage()));
        }
        else {
            err.println(cmd.getColorScheme().errorText(e.getClass().getName() + " : " + e.getMessage()));
        }
        return cmd.getExitCodeExceptionMapper() != null
                ? cmd.getExitCodeExceptionMapper().getExitCode(e)
                : cmd.getCommandSpec().exitCodeOnExecutionException();
    }
}
