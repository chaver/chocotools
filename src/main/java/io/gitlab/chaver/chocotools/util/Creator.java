package io.gitlab.chaver.chocotools.util;

/**
 * Interface to create an object of type S
 * @param <S> type of the object
 */
public interface Creator<S> {

    S create();
}
