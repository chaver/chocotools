package io.gitlab.chaver.chocotools.util;

import java.util.List;

/**
 * Provider of a list of solution type S
 * @param <S> type of solution
 */
public interface ISolutionProvider<S> {

    List<S> getSolutions();
}
