package io.gitlab.chaver.chocotools.problem;

/**
 * Catch invalid arguments in the command line
 */
public class SetUpException extends Exception {

    public SetUpException(String message) {
        super(message);
    }

    public SetUpException(String message, Throwable cause) {
        super(message, cause);
    }
}
