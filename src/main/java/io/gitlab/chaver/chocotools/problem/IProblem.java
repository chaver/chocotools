package io.gitlab.chaver.chocotools.problem;

import java.util.List;

/**
 * Represents a problem to solve with a model (ex: LP, CP)
 * @param <M> type of the model of the problem
 * @param <S> type of the solutions of the problem
 * @param <P> type of the properties of the problem
 */
public interface IProblem<M, S, P> {

    /**
     * Set up the problem (create the model and parse args)
     * @throws SetUpException if an argument is incorrect
     */
    void setUp() throws SetUpException;

    /**
     * Build model of the problem (add variables/constraints)
     * @throws BuildModelException if building the model fails
     */
    void buildModel() throws BuildModelException;

    /**
     * Solve the problem
     * @throws SolvingException if solving the problem fails
     */
    void solve() throws SolvingException;

    /**
     * Output result of the problem (print solutions/stats and save them)
     * @throws OutputException if outputting the result fails
     */
    void outputResult() throws OutputException;

    /**
     * Get model of the problem
     * @return model of the problem
     */
    M getModel();

    /**
     * Get solutions of the problem
     * @return list of solutions of the problem
     */
    List<S> getSolutions();

    /**
     * Get properties of the problem (ex: search stats)
     * @return properties of the problems
     */
    P getProperties();
}
