package io.gitlab.chaver.chocotools.problem;

/**
 * Catch exception while building the model
 */
public class BuildModelException extends Exception {

    public BuildModelException(String message) {
        super(message);
    }

    public BuildModelException(String message, Throwable cause) {
        super(message, cause);
    }
}
