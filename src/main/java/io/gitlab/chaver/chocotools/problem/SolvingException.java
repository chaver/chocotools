package io.gitlab.chaver.chocotools.problem;

/**
 * Catch exception during the solving of the problem
 */
public class SolvingException extends Exception {

    public SolvingException(String message) {
        super(message);
    }

    public SolvingException(String message, Throwable cause) {
        super(message, cause);
    }
}
