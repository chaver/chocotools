package io.gitlab.chaver.chocotools.problem;

import io.gitlab.chaver.chocotools.io.MeasuresView;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;


/**
 * Represents a CP problem to solve with Choco solver
 * @param <S> type of the solutions of the problem
 * @param <P> type of the properties of the problem
 */
public abstract class ChocoProblem<S, P> extends AbstractProblem<Model, S, P> {

    protected Solver solver;

    @Override
    public void setUp() throws SetUpException {
        model = createModel();
        solver = model.getSolver();
        if (timeLimit != null) {
            if (timeLimit <= 0) {
                throw new SetUpException("Time limit must be an integer > 0");
            }
            solver.limitTime(timeLimit + "s");
        }
        parseArgs();
    }

    @Override
    public void solve() {
        while (solver.solve());
    }

    @Override
    protected void printStats() {
        System.out.println("-------------------------------------------------------------------------------------");
        solver.printStatistics();
    }

    /**
     * Get search stats of the search
     * @return search stats of the search
     */
    public MeasuresView getSearchStats() {
        return new MeasuresView(solver.getMeasures());
    }

    /**
     * Create the model with the adapted settings, the constraints and the variables should be created in buildModel()
     * @return model of the problem
     */
    protected abstract Model createModel();

    /**
     * Parse command line arguments (ex: read a database)
     * @throws SetUpException if set up fails
     */
    protected abstract void parseArgs() throws SetUpException;
}
