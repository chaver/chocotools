package io.gitlab.chaver.chocotools.problem;

/**
 * Catch exception while outputting the result
 */
public class OutputException extends Exception {

    public OutputException(String message) {
        super(message);
    }

    public OutputException(String message, Throwable cause) {
        super(message, cause);
    }
}
