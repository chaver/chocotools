package io.gitlab.chaver.chocotools.problem;

import io.gitlab.chaver.chocotools.io.JsonLinesResultWriter;
import io.gitlab.chaver.chocotools.io.ProblemResult;
import io.gitlab.chaver.chocotools.io.JsonResultWriter;
import picocli.CommandLine.Option;

import java.io.IOException;
import java.util.concurrent.Callable;


/**
 * Represents an abstract problem to solve with a model
 * @param <M> type of the model of the problem
 * @param <S> type of the solutions of the problem
 * @param <P> type of the properties of the problem
 */
public abstract class AbstractProblem<M, S, P> implements IProblem<M, S, P>, Callable<Integer> {

    @Option(names = "--tl", description = "Time limit of the search (seconds)")
    protected Integer timeLimit;
    @Option(names = "-s", description = "Print search stats")
    protected boolean printStats;
    @Option(names = "-p", description = "Print solutions")
    protected boolean printSolutions;
    @Option(names = "--json", description = "Save result in JSON format")
    protected String jsonPath;
    @Option(names = "--jsonl", description = "Save result in JSON lines format")
    protected String jsonLinesPath;
    @Option(names = "--skips", description = "Skip solutions in the save")
    protected boolean skipSolutions;

    protected M model;

    /**
     * Print solutions of the problem
     * @throws OutputException if outputting error
     */
    protected void printSolutions() throws OutputException {
        getSolutions().forEach(System.out::println);
    }

    /**
     * Print stats of the search
     * @throws OutputException if outputting error
     */
    protected abstract void printStats() throws OutputException;

    /**
     * Save result in JSON format
     * @throws IOException if writing error
     */
    protected void saveResultJson() throws IOException {
        ProblemResult<S, P> result = new ProblemResult<>(getSolutions(), getProperties());
        new JsonResultWriter<S, P>(jsonPath).writeResult(result, skipSolutions);
    }

    protected void saveResultJsonLines() throws IOException {
        ProblemResult<S, P> result = new ProblemResult<>(getSolutions(), getProperties());
        new JsonLinesResultWriter<S, P>(jsonLinesPath).writeResult(result, skipSolutions);
    }

    @Override
    public void outputResult() throws OutputException {
        if (printSolutions) {
            printSolutions();
        }
        if (printStats) {
            printStats();
        }
        if (jsonPath != null) {
            try{
                saveResultJson();
            } catch (IOException e) {
                throw new OutputException(e.getMessage(), e);
            }
        }
        if (jsonLinesPath != null) {
            try {
                saveResultJsonLines();
            } catch (IOException e) {
                throw new OutputException(e.getMessage(), e);
            }
        }
    }

    @Override
    public M getModel() {
        return model;
    }

    @Override
    public Integer call() throws Exception {
        setUp();
        buildModel();
        solve();
        outputResult();
        return 0;
    }
}
