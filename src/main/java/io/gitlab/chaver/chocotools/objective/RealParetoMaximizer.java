package io.gitlab.chaver.chocotools.objective;

import io.gitlab.chaver.chocotools.util.Creator;
import io.gitlab.chaver.chocotools.util.ISolutionProvider;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.RealVar;
import org.chocosolver.util.ESat;

import java.util.LinkedList;
import java.util.List;

/**
 * Pareto front with real variables
 * @param <S> solution type
 */
public abstract class RealParetoMaximizer<S>
        extends Propagator<RealVar>
        implements IMonitorSolution, ISolutionProvider<S> {

    private RealVar[] objectives;
    private List<S> archive = new LinkedList<>();
    private Creator<S> creator;
    private boolean strict;
    /** Precision of the real variables */
    private double varPrecision;

    public RealParetoMaximizer(RealVar[] objectives, Creator<S> creator, double varPrecision, boolean strict) {
        super(objectives, PropagatorPriority.QUADRATIC, false);
        this.objectives = objectives;
        this.creator = creator;
        this.strict = strict;
        this.varPrecision = varPrecision;
    }

    @Override
    public List<S> getSolutions() {
        return archive;
    }

    /**
     * Compute dominated point for objective i,
     *  i.e. DP_i = (obj_1_max,...,obj_i_min,...,obj_m_max)
     * @param i index of the variable
     * @return dominated point
     */
    private double[] computeDominatedPoint(int i) {
        double[] DP = new double[objectives.length];
        for (int j = 0; j < objectives.length; j++) {
            RealVar currentVar = objectives[j];
            DP[j] = j == i ? currentVar.getLB() : currentVar.getUB();
        }
        return DP;
    }

    protected abstract double[] getObjValues(S sol);

    /**
     * Compute tightest point for objective i
     *  i.e. the point that dominates DP_i and has the biggest obj_i
     * @param i index of the variable
     */
    private void computeTightestPoint(int i) throws ContradictionException {
        double tightestPoint = Double.MIN_VALUE;
        double[] dominatedPoint = computeDominatedPoint(i);
        for (S s : archive) {
            double[] sol = getObjValues(s);
            int dominates = dominates(sol, dominatedPoint, i);
            if (dominates > 0) {
                double currentPoint = (dominates == 1 && !strict) ? sol[i] : sol[i] + varPrecision;
                if (tightestPoint < currentPoint) {
                    tightestPoint = currentPoint;
                }
            }
        }
        if (tightestPoint > Double.MIN_VALUE) {
            objectives[i].updateLowerBound(tightestPoint, this);
        }
    }

    /**
     * Return an int :
     *  0 if a doesn't dominate b
     *  1 if a dominates b and a = b if we don't take into account index i
     *  2 if a dominates b and a dominates b if we don't take into account index i
     * @param a vector
     * @param b vector
     * @param i index
     * @return an int representing the fact that a dominates b
     */
    private int dominates(double[] a, double[] b, int i) {
        int dominates = 0;
        for (int j = 0; j < objectives.length; j++) {
            if (a[j] < b[j]) return 0;
            if (a[j] > b[j]) {
                if (dominates == 0) dominates = 1;
                if (j != i) dominates = 2;
            }
        }
        return dominates;
    }


    @Override
    public void propagate(int evtmask) throws ContradictionException {
        for (int i = 0; i < objectives.length; i++) {
            computeTightestPoint(i);
        }
    }

    @Override
    public void onSolution() {
        S sol = creator.create();
        archive.removeIf(s2 -> dominates(getObjValues(sol), getObjValues(s2), 0) > 0);
        archive.add(sol);
    }

    @Override
    public ESat isEntailed() {
        return ESat.TRUE;
    }
}
