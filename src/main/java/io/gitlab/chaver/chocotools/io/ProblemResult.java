package io.gitlab.chaver.chocotools.io;


import java.util.List;

/**
 * Result of a problem
 * @param <S> type of the solutions of the problem
 * @param <P> type of the properties of the problem
 */
public class ProblemResult<S, P> {

    private List<S> solutions;
    private P properties;

    public ProblemResult(List<S> solutions, P properties) {
        this.solutions = solutions;
        this.properties = properties;
    }

    public ProblemResult(P properties) {
        this.properties = properties;
    }

    public List<S> getSolutions() {
        return solutions;
    }

    public P getProperties() {
        return properties;
    }
}
