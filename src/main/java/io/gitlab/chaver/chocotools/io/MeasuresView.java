package io.gitlab.chaver.chocotools.io;

import org.chocosolver.solver.search.SearchState;
import org.chocosolver.solver.search.measure.Measures;

/**
 * A view for Measures class of Choco solver
 */
public class MeasuresView {

    protected SearchState state;
    protected long solutionCount;
    protected float timeCount;
    protected float readingTimeCount;
    protected float timeToBestSolution;
    protected long nodeCount;
    protected long backtrackCount;
    protected long backjumpCount;
    protected long restartCount;
    protected long failCount;
    protected long bestSolutionCount;


    public MeasuresView() {}

    public MeasuresView(Measures measures) {
        state = measures.getSearchState();
        solutionCount = measures.getSolutionCount();
        timeCount = measures.getTimeCount();
        readingTimeCount = measures.getReadingTimeCount();
        timeToBestSolution = measures.getTimeToBestSolution();
        nodeCount = measures.getNodeCount();
        backtrackCount = measures.getBackTrackCount();
        backjumpCount = measures.getBackjumpCount();
        restartCount = measures.getRestartCount();
        failCount = measures.getFailCount();
        bestSolutionCount = solutionCount;
    }

    public SearchState getState() {
        return state;
    }

    public void setState(SearchState state) {
        this.state = state;
    }

    public long getSolutionCount() {
        return solutionCount;
    }

    public void setSolutionCount(long solutionCount) {
        this.solutionCount = solutionCount;
    }

    public float getTimeCount() {
        return timeCount;
    }

    public void setTimeCount(float timeCount) {
        this.timeCount = timeCount;
    }

    public float getReadingTimeCount() {
        return readingTimeCount;
    }

    public void setReadingTimeCount(float readingTimeCount) {
        this.readingTimeCount = readingTimeCount;
    }

    public float getTimeToBestSolution() {
        return timeToBestSolution;
    }

    public void setTimeToBestSolution(float timeToBestSolution) {
        this.timeToBestSolution = timeToBestSolution;
    }

    public long getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(long nodeCount) {
        this.nodeCount = nodeCount;
    }

    public long getBacktrackCount() {
        return backtrackCount;
    }

    public void setBacktrackCount(long backtrackCount) {
        this.backtrackCount = backtrackCount;
    }

    public long getBackjumpCount() {
        return backjumpCount;
    }

    public void setBackjumpCount(long backjumpCount) {
        this.backjumpCount = backjumpCount;
    }

    public long getRestartCount() {
        return restartCount;
    }

    public void setRestartCount(long restartCount) {
        this.restartCount = restartCount;
    }

    public long getFailCount() {
        return failCount;
    }

    public void setFailCount(long failCount) {
        this.failCount = failCount;
    }

    public long getBestSolutionCount() {
        return bestSolutionCount;
    }

    public void setBestSolutionCount(long bestSolutionCount) {
        this.bestSolutionCount = bestSolutionCount;
    }

    @Override
    public String toString() {
        return "MeasuresView{" +
                "state=" + state +
                ", solutionCount=" + solutionCount +
                ", timeCount=" + timeCount +
                ", readingTimeCount=" + readingTimeCount +
                ", timeToBestSolution=" + timeToBestSolution +
                ", nodeCount=" + nodeCount +
                ", backtrackCount=" + backtrackCount +
                ", backjumpCount=" + backjumpCount +
                ", restartCount=" + restartCount +
                ", failCount=" + failCount +
                ", bestSolutionCount=" + bestSolutionCount +
                '}';
    }
}
