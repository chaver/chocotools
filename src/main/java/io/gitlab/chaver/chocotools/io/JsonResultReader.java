package io.gitlab.chaver.chocotools.io;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * JSON reader for result of a problem
 */
public class JsonResultReader<S, P> extends ProblemResultReader<S, P> {

    public JsonResultReader(String path) {
        super(path);
    }

    @Override
    public ProblemResult<S, P> readResult(Class<S[]> solutionArrayClass, Class<P> propClass) throws IOException {
        JsonObject root = new Gson().fromJson(new BufferedReader(new FileReader(path)), JsonObject.class);
        List<S> solutions = root.has("solutions") ?
                Arrays.asList(new Gson().fromJson(root.get("solutions"), solutionArrayClass)) : null;
        P properties = new Gson().fromJson(root.get("properties"), propClass);
        return new ProblemResult<>(solutions, properties);
    }
}
