package io.gitlab.chaver.chocotools.io;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * JSON writer for ProblemResult
 */
public class JsonResultWriter<S, P> extends ProblemResultWriter<S, P> {

    public JsonResultWriter(String path) {
        super(path);
    }

    @Override
    public void writeResult(ProblemResult<S, P> result, boolean skipSolutions) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes field) {
                    return field.getName().equals("solutions") && skipSolutions;
                }
                @Override
                public boolean shouldSkipClass(Class<?> aClass) {
                    return false;
                }
            }).create();
            writer.write(gson.toJson(result));
        }
    }
}
