package io.gitlab.chaver.chocotools.io;

import java.io.IOException;

/**
 * Reader for ProblemResult
 * @param <S> type of the solutions of the problem
 * @param <P> type of the properties of the problem
 */
public abstract class ProblemResultReader<S, P> {

    protected String path;

    public ProblemResultReader(String path) {
        this.path = path;
    }

    /**
     * Read ProblemResult
     * @param solutionArrayClass class of the array of solutions, for instance,
     *                     if the class is Foo then solutionType = Foo[].class
     * @param propClass class of the properties
     * @return ProblemResult in the file
     * @throws IOException if reading error
     */
    public abstract ProblemResult<S, P> readResult(Class<S[]> solutionArrayClass, Class<P> propClass) throws IOException;
}
