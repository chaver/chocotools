package io.gitlab.chaver.chocotools.io;

import java.io.IOException;

/**
 * Writer for ProblemResult
 * @param <S> type of the solutions of the problem
 * @param <P> type of the properties of the problem
 */
public abstract class ProblemResultWriter<S, P> {

    protected String path;

    public ProblemResultWriter(String path) {
        this.path = path;
    }

    /**
     * Write a ProblemResult in a file
     * @param result result to write
     * @param skipSolutions true if we don't want to write the solutions in the file
     * @throws IOException if writing error
     */
    public abstract void writeResult(ProblemResult<S, P> result, boolean skipSolutions) throws IOException;

}
