package io.gitlab.chaver.chocotools.io;

import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Write result in JSON lines format (see <a href="https://jsonlines.org/">jsonlines</a>)
 */
public class JsonLinesResultWriter<S, P> extends ProblemResultWriter<S, P> {

    public JsonLinesResultWriter(String path) {
        super(path);
    }

    @Override
    public void writeResult(ProblemResult<S, P> result, boolean skipSolutions) throws IOException {
        String propertiesPath = path + "_prop.jsonl";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(propertiesPath))) {
            new Gson().toJson(result.getProperties(), writer);
        }
        if (!skipSolutions) {
            String solsPath = path + "_sols.jsonl";
            Gson gson = new Gson();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(solsPath))) {
                for (S sol : result.getSolutions()) {
                    writer.write(gson.toJson(sol) + "\n");
                }
            }
        }
    }
}
