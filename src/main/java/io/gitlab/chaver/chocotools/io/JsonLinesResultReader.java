package io.gitlab.chaver.chocotools.io;

import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Read result in JSON lines format (see <a href="https://jsonlines.org/">jsonlines</a>)
 */
public class JsonLinesResultReader<S, P> extends ProblemResultReader<S, P> {

    private Class<S> solutionClass;

    public JsonLinesResultReader(String path, Class<S> solutionClass) {
        super(path);
        this.solutionClass = solutionClass;
    }

    @Override
    public ProblemResult<S, P> readResult(Class<S[]> solutionArrayClass, Class<P> propClass) throws IOException {
        String propertiesPath = path + "_prop.jsonl";
        Gson gson = new Gson();
        BufferedReader reader = new BufferedReader(new FileReader(propertiesPath));
        P properties = gson.fromJson(reader, propClass);
        reader.close();
        String solsPath = path + "_sols.jsonl";
        reader = new BufferedReader(new FileReader(solsPath));
        String line;
        List<S> solutions = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            solutions.add(gson.fromJson(line, solutionClass));
        }
        return new ProblemResult<>(solutions, properties);
    }
}
