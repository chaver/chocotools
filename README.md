# Chocotools

## Requirements

- Java 8+
- Maven 3

## Description

A set of tools to facilitate constraint programming with [Choco solver](https://choco-solver.org/) and with any LP/CP solver in general. The principle is the following : we standardise the steps that are commons to most of the problems, to avoid repeating multiple times similar code. It uses [Picocli](https://picocli.info/) for command-line args parsing.

It can be added as a maven dependency :

```xml
<dependency>
    <groupId>io.gitlab.chaver</groupId>
    <artifactId>chocotools</artifactId>
    <version>1.1.2</version>
</dependency>
```

The typical workflow to solve a CP/LP problem is the following :

1. Set-up the problem (create the model and parse command line args)
2. Build the model (add variables/constraints/objectives, configure search strategy)
3. Solve the problem
4. Output the result (print solutions/stats, write result in a file)

The interface `IProblem` represents a problem to solve and provides the following methods :

- `void setUp()` : step 1 of the workflow, throws a `SetUpException` if set-up fails
- `void buildModel()` : step 2 of the workflow, throws a `BuildModelException` if building the model fails
- `void solve()` : step 3 of the workflow, throws a `SolvingException` if solving the problem fails
- `void outputResult()` : step 4 of the workflow, throws a `OutputException` if outputting the result fails
- `M getModel()` : return the model of the problem of generic type `M`
- `List<S> getSolutions()` : return a list of generic type `S` which contains the (best) solutions of the problem
- `P getProperties()` : return the properties of the problem of generic type `P`, for example, the search stats (number of nodes/fails/solutions, time to find all the solutions, ...)

The abstract class `AbstractProblem` is a subclass of `IProblem` with fields annotated with `@Option` which means that they can be initialised in a command-line. The following args can be specified in any class that extends `AbstractProblem` :

- `--tl=<timeLimit>` : Time limit of the search (seconds)
- `-p` : Print solutions
- `-s` : Print search stats
- `--json=<jsonPath>` : Path of the JSON file where to save the result
- `--jsonl=<jsonPath>` : Path where to save the result in JSON lines format
- `--skips` : Save only the properties of the problem and not the solutions

 It provides the following methods :

- `void printSolutions()` : Print the (best) solutions, can throw a `OutputException`
- `abstract void printStats()` : Print the search stats, can throw a `OutputException`
- `void saveResultJson()` : Save the result in JSON format, can throw a `IOException`

`AbstractProblem` implements the method `outputResult()` by calling the three methods defined above accordingly to the command-line args : for example, if `-p` was specified, then the method `printSolutions()` will be called. It also implements the method `call()` from the interface `Callable` : this method calls all the workflow methods defined above in the correct order. 

Finally, the abstract class `ChocoProblem` is a subclass of `AbstractProblem` and provides the following methods :

- `MeasuresView getSearchStats()` : return the search stats of the problem (number of nodes/fails/solutions, time, ...)
- `abstract Model createModel()` : create the model of the problem and return it
- `abstract void parseArgs()` : parse the command-line args (when Picocli can't do the job, for example, check if a value is correct or read a dataset), throws `SetUpException` if an argument is not correct

`ChocoProblem` implements the method `setUp()` by first calling `createModel()`, set-up the timeout of the search and then calls the method `parseArgs()`.

## Simple use case

To illustrate the interest of using Chocotools, we propose a simple use-case here.

Let `x` and `y` be two integer variables. We want to find all the values such that `x < y`. The upper bounds of `x` and `y` are provided by the user. First, we need to create a class which represents a solution of this problem :

```java
import java.util.Objects;

public class SolutionExample {

    private int x;
    private int y;

    public SolutionExample(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return x + " " + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SolutionExample that = (SolutionExample) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}

```

 The class to solve this problem is the following :

```java
import io.gitlab.chaver.chocotools.io.MeasuresView;
import io.gitlab.chaver.chocotools.problem.ChocoProblem;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;
import picocli.CommandLine;

import java.util.LinkedList;
import java.util.List;

public class ChocoProblemExample extends ChocoProblem<SolutionExample, MeasuresView> {

    private static class SolMonitor implements IMonitorSolution {

        private IntVar x;
        private IntVar y;
        private List<SolutionExample> sols = new LinkedList<>();

        public SolMonitor(IntVar x, IntVar y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void onSolution() {
            sols.add(new SolutionExample(x.getValue(), y.getValue()));
        }

        public List<SolutionExample> getSols() {
            return sols;
        }
    }

    private SolMonitor solMonitor;
    @Option(names = "--xmax", description = "Upper bound of x", required=true)
    private int xmax;
    @Option(names = "--ymax", description = "Upper bound of y", required=true)
    private int ymax;

    @Override
    public List<SolutionExample> getSolutions() {
        return solMonitor.getSols();
    }

    @Override
    public MeasuresView getProperties() {
        return getSearchStats();
    }

    @Override
    public void parseArgs() {}

    @Override
    public void buildModel() {
        IntVar x = model.intVar("x", 1, xmax);
        IntVar y = model.intVar("y", 2, ymax);
        x.lt(y).post();
        solMonitor = new SolMonitor(x, y);
        solver.plugMonitor(solMonitor);
    }

    @Override
    protected Model createModel() {
        return new Model("example");
    }
    
    public static void main(String[] args) {
        int exitCode = new CommandLine(new ChocoProblemExample()).execute(args);
        System.exit(exitCode);
    }
}

```

Let us describe the `ChocoProblemExample` class. First, it extends the abstract class `ChocoProblem` with two generic types :

- `SolutionExample` : type of the solutions of the problem, see above
- `MeasuresView` : type of properties of the problem, this class represents the measures of the solver (number of nodes/fails, etc...)

Then, we define a new class `SolMonitor` which implements `IMonitorSolution` (see the documentation of Choco for more information). This class is used to store all the solutions we find during the search. The fields `xmax` and `ymax` are annotated with the `@Option` annotation which means that they can be specified by the user in the command-line. The body of the method `parseArgs()` is empty since we don't need to check anything here. Finally, we can call the class `ChocoProblemExample` in command-line with the `main` method.

As you can see, we can focus on the code of the model and we don't have to specify how to print/save the result since it is standardised. For example, if we call the program with the following args : `--xmax 3 --ymax 3 --json res.json`, we will get a JSON file similar to this (unminified here to make it clearer) :

```json
{
  "solutions": [
    {
      "x": 1,
      "y": 2
    },
    {
      "x": 1,
      "y": 3
    },
    {
      "x": 2,
      "y": 3
    }
  ],
  "properties": {
    "state": "TERMINATED",
    "solutionCount": 3,
    "timeCount": 0.03,
    "readingTimeCount": 0.09,
    "timeToBestSolution": 0.02,
    "nodeCount": 5,
    "backtrackCount": 5,
    "backjumpCount": 0,
    "restartCount": 0,
    "failCount": 0,
    "bestSolutionCount": 3
  }
}
```

## Package organisation

The packages of the project are the following :

- `io` : contains all the classes related to Input/Output such as  `ProblemResult` which represents the result of a problem, `ProblemResultReader` which can be used to read a result or `ProblemResultWriter` to write a result
- `problem` : contains the Problem classes described above
- `util` : other classes related for example to print errors with Picocli

## Questions/suggestions

Feel free to contact me for any questions/suggestions related to this project : [@Charles Vernerey](mailto:charlesvernerey2@gmail.com) 